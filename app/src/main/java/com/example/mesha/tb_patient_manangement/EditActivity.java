package com.example.mesha.tb_patient_manangement;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.InputType;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EditActivity extends AppCompatActivity {
    ArrayList<String> clinicList;
    ArrayList<String> genderList;
    private Spinner clinics;
    private Spinner gender;
    private EditText name;
    private EditText surname;
    private Button editPatient;
    private Button cancel;
    private EditText cellNo;
    private EditText yearOfBirth;
    private EditText addContact;
    private DatabaseReference mReff;
    private DatabaseReference contact_num;
    private DatabaseReference roofRef;
    private String logedUser;
    private boolean exists;

    private ArrayAdapter genderAdapter;
    private ArrayAdapter clinicAdapter;

    private String key;
    private String clinic;

    private ConstraintLayout parentLayout;
    private int hint=0;
    ArrayList<String> numbers;
    private ArrayList<TextView>textList= new ArrayList<TextView>();

    ClientRecyclerViewAdapter clientRecyclerViewAdapter;

    private int contacts_count = 1;

    private HashMap<String, String> contacts_name;

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(getBaseContext(), Menue.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //intent.putExtra("PatientEamil", logedUser);
        //intent.putExtra("PATIENTID", patientId);
        intent.putExtra("clinic", clinic);
        intent.putExtra("PatientEamil", logedUser);

        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_edit_form);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Intent intent1= getIntent();
        logedUser=intent1.getStringExtra("PatientEamil");
        clinic = intent1.getStringExtra("clinic");
        roofRef = FirebaseDatabase.getInstance().getReference();
        mReff= FirebaseDatabase.getInstance().getReference().child("TB");

        Intent intent = getIntent();
        String client_number = intent.getStringExtra("client_number");
        String client_name = intent.getStringExtra("client_name");
        String client_surname = intent.getStringExtra("client_surname");
        String client_year = intent.getStringExtra("client_year");
        String client_sex = intent.getStringExtra("client_sex");
        String client_clinic = intent.getStringExtra("client_clinic");

        clinic = intent.getStringExtra("clinic");

        // mReff= new firebase()
        clinics= (Spinner) findViewById(R.id.clinicDropDown);
        gender=(Spinner) findViewById(R.id.genderDropdown);

        clinics.setEnabled(false);
        gender.setEnabled(false);

        name= (EditText) findViewById(R.id.names);
        surname= (EditText) findViewById(R.id.surname);
        cellNo= (EditText) findViewById(R.id.cellNo);
        yearOfBirth = (EditText) findViewById(R.id.yearOfBirth);
        addContact = (EditText) findViewById(R.id.addContact);
        editPatient= (Button) findViewById(R.id.clientAddButton);
        cancel= (Button) findViewById(R.id.clientCancelButton);

        genderList= new ArrayList<String>();
        genderList.add(client_sex);

        clinicList = new ArrayList<String>();
        clinicList.add(client_clinic);

        parentLayout = (ConstraintLayout)findViewById(R.id.editLayout);

        genderAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, genderList);
        clinicAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, clinicList);
        clinics.setAdapter(clinicAdapter);
        gender.setAdapter(genderAdapter);

        numbers = new ArrayList<>();

        contacts_name = new HashMap<String, String>();

        populateClient(getBaseContext(), client_number, client_name, client_surname, client_year, client_sex);

        RecyclerView recyclerView = findViewById(R.id.contact_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        clientRecyclerViewAdapter = new ClientRecyclerViewAdapter(this, numbers);
        recyclerView.setAdapter(clientRecyclerViewAdapter);

        editPatient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String cli= clinics.getSelectedItem().toString();
                String no= cellNo.getText().toString();
                String gender1= gender.getSelectedItem().toString();
                String firstName= name.getText().toString();
                String secondName=surname.getText().toString();
                String yob = yearOfBirth.getText().toString();
                final String add = addContact.getText().toString();

                if(!add.equals("")) {
                    if (add.length() != 10){
                        Toast.makeText(getApplicationContext(), "Enter valid contact number", Toast.LENGTH_SHORT).show();}
                    else {
                        // Add method to check if Number already exists in the list
                        if(contacts_name.containsValue(add)) {
                            Toast.makeText(getApplicationContext(), add+ " already exists", Toast.LENGTH_SHORT).show();
                        }else{
                            contacts_name.put(("Contact" + contacts_count), add);

                            mReff.child(key).child("clin").setValue(cli);
                            mReff.child(key).child("sex").setValue(gender1);
                            mReff.child(key).child("yearOfBirth").setValue(yob);
                            mReff.child(key).child("yourFirstName").setValue(firstName.toUpperCase());
                            mReff.child(key).child("yourSecondName").setValue(secondName.toUpperCase());
                            mReff.child(key).child("cell").setValue(no);
                            mReff.child(key).child("Contact Numbers").setValue(contacts_name);

                            add_to_contact_list(add);

                        }

                    }
                }
                else {
                    Toast.makeText(getApplicationContext(), "Please enter a valid number", Toast.LENGTH_SHORT).show();
                    mReff.child(key).child("clin").setValue(cli);
                    mReff.child(key).child("sex").setValue(gender1);
                    mReff.child(key).child("yearOfBirth").setValue(yob);
                    mReff.child(key).child("yourFirstName").setValue(firstName.toUpperCase());
                    mReff.child(key).child("yourSecondName").setValue(secondName.toUpperCase());
                    mReff.child(key).child("cell").setValue(no);

                }



                Intent intent = new Intent(getBaseContext(), EditActivity.class);
                intent.putExtra("client_number", no);
                intent.putExtra("client_name", firstName.toUpperCase());
                intent.putExtra("client_surname", secondName.toUpperCase());
                intent.putExtra("client_year", yob);
                intent.putExtra("client_sex", gender1);
                intent.putExtra("client_clinic", cli);
                intent.putExtra("clinic", cli);
                startActivity(intent);
                finish();
            }

        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), SearchActivity.class);
                intent.putExtra("clinic", clinic);
                startActivity(intent);
                finish();
            }
        });

    }

    public void populateClient(final Context text, final String cellNumber, final String client_name, final String client_surname, final String client_year, final String client_sex){
        final ArrayList<String> clientList =  new ArrayList<String>();

        mReff.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                fetchClients(dataSnapshot, cellNumber, clientList, client_name, client_surname, client_year, client_sex);

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                //fetchDta(dataSnapshot,clinicLists);
                fetchClients(dataSnapshot, cellNumber, clientList, client_name, client_surname, client_year, client_sex);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void fetchClients(DataSnapshot snapshot, String cell, ArrayList<String> clinicLists, String client_name, String client_surname, String client_year, String client_sex){

        if( snapshot.child("cell").getValue().equals(cell) &&
                snapshot.child("yourFirstName").getValue().equals(client_name) &&
                snapshot.child("yourSecondName").getValue().equals(client_surname) &&
                snapshot.child("yearOfBirth").getValue().equals(client_year) &&
                snapshot.child("sex").getValue().equals(client_sex) ){

            key = snapshot.getKey().toString();

            cellNo.setText( cell );
            name.setText( snapshot.child("yourFirstName").getValue().toString() );
            surname.setText( snapshot.child("yourSecondName").getValue().toString() );
            yearOfBirth.setText( snapshot.child("yearOfBirth").getValue().toString() );

            if( snapshot.child("Contact Numbers").getValue() != null ) {

                for(DataSnapshot contact : snapshot.child("Contact Numbers").getChildren()) {
                    numbers.add(contact.getValue().toString());
                    contacts_name.put(("Contact" + contacts_count), contact.getValue().toString());
                    contacts_count++;
                }
                System.out.println("clients "+contacts_count);

            }
        }


    }

    private void add_to_contact_list(final String phoneNumber){
        contact_num = FirebaseDatabase.getInstance().getReference().child("Contact List");

        roofRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                int check_in_contact_list = 0;
                int check_in_patient_list = 0;

                long timestamp = new Date().getTime();
                ContactInfo potential = new ContactInfo(timestamp, timestamp, 1, "Not smsed", false, false);

                if(dataSnapshot.child("Contact List").exists()){

                    for (DataSnapshot data : dataSnapshot.child("TB").getChildren()) {
                        if(data.child("cell").getKey().equals(phoneNumber)) {
                            check_in_patient_list++;
                            break;
                        }
                    }

                    if(check_in_patient_list == 0) {
                        for (DataSnapshot data : dataSnapshot.child("Contact List").getChildren()) {
                            if (data.getKey().equals(phoneNumber)) {
                                check_in_contact_list++;
                                break;
                            }
                        }

                        if (check_in_contact_list == 0) {
                            contact_num.child(phoneNumber).setValue(potential);
                        } else {
                            DataSnapshot con = dataSnapshot.child("Contact List").child(phoneNumber);
                            int diff = (int) (new Date().getTime() - (long) con.child("first_timestamp").getValue());
                            int days = (diff / (1000 * 60 * 60 * 24));
                            int count = Integer.parseInt(con.child("count").getValue() + "") + 1;

                            contact_num.child(phoneNumber).child("count").setValue(count);

                            if (days > 30 && (boolean) con.child("smsed").getValue() && !(boolean) con.child("closed_loop").getValue()) {
                                contact_num.child(phoneNumber).child("first_timestamp").setValue(new Date().getTime());
                                contact_num.child(phoneNumber).child("smsed").setValue(false);
                            }

                        }
                    }
                }
                else
                    contact_num.child(phoneNumber).setValue(potential);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public class ClientRecyclerViewAdapter extends RecyclerView.Adapter<ClientViewHolder>{

        ArrayList<String> mData;
        private LayoutInflater mInflator;

        public ClientRecyclerViewAdapter(Context context, ArrayList<String> numbers) {
            this.mInflator = LayoutInflater.from(context);
            this.mData = numbers;
        }

        @NonNull
        @Override
        public ClientViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(parentLayout.getContext()).inflate(R.layout.activity_contacts_client, parentLayout, false);

            return  new ClientViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ClientViewHolder viewHolder, int i) {
            viewHolder.setDetails(mData.get(i));
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

    }

    //view holder class
    public class ClientViewHolder extends RecyclerView.ViewHolder{

        View cView;

        public ClientViewHolder(@NonNull View itemView) {
            super(itemView);

            cView = itemView;
        }

        public void setDetails(String cell){
            TextView cell_num = (TextView)cView.findViewById(R.id.textCell);
            cell_num.setText(cell);
        }

    }
    public void getSameValue(String v){
        for(String s : contacts_name.values()) {
            if (v.matches(s)) {
                Toast.makeText(getApplicationContext(),s+" Already exists",Toast.LENGTH_SHORT).show();
                exists=true;
            }
        }
        if(exists){

        }else{
            mReff.child(key).child("Contact Numbers").setValue(contacts_name);
            Toast.makeText(getApplicationContext(), "Client edited Successfully", Toast.LENGTH_SHORT).show();
        }
    }
}
