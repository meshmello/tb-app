package com.example.mesha.tb_patient_manangement;

import android.media.TimedMetaData;

import java.util.ArrayList;
import java.util.Objects;

public class PatientInfo {

    public String clin;
    public String cell;
    public String altNumber;
    public String sex;
    public String yourFirstName;
    public String yourSecondName;
    public String yearOfBirth;
    public String noOfCards;
    public String consent;
    public long timestamp;
    public ArrayList<String> clinics;


    public PatientInfo(){

    }

    public String getClin() {
        return clin;
    }

    public void setClin(String clin) {
        this.clin = clin;
    }

    public String getCell() {
        return cell;
    }

    public void setCell(String cell) {
        this.cell = cell;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getYourFirstName() {
        return yourFirstName;
    }

    public void setYourFirstName(String yourFirstName) {
        this.yourFirstName = yourFirstName;
    }

    public String getYourSecondName() {
        return yourSecondName;
    }

    public void setYourSecondName(String yourSecondName) {
        this.yourSecondName = yourSecondName;
    }

    public String getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(String yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public ArrayList<String> getClinics() {
        return clinics;
    }
    public  String getNoOfCards(){
        return noOfCards;
    }

    public String getConsent() {
        return consent;
    }

    public void setNoOfCards(String noOfCards) {
        this.noOfCards = noOfCards;
    }
    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public void setClinics(ArrayList<String> clinics) {
        this.clinics = clinics;
    }

    public void setConsent(String consent) {
        this.consent = consent;
    }

    public PatientInfo(String consent, String clin, String cell, String altNumber, String sex, String yourFirstName, String yourSecondName, String yearOfBirth, String noOfCards, long timestamp) {
        this.consent=consent;
        this.clin = clin;
        this.cell = cell;
        this.altNumber=altNumber;
        this.sex = sex;
        this.yourFirstName = yourFirstName;
        this.yourSecondName = yourSecondName;
        this.yearOfBirth = yearOfBirth;
        this.noOfCards=noOfCards;
        this.timestamp=timestamp;
    }
    public PatientInfo(ArrayList<String>clinics){
        this.clinics = clinics;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PatientInfo)) return false;
        PatientInfo that = (PatientInfo) o;


        return //Objects.equals(clin, that.clin) &&
                Objects.equals(cell, that.cell) &&
                Objects.equals(sex, that.sex) &&
                Objects.equals(yourFirstName, that.yourFirstName) &&
                Objects.equals(yourSecondName, that.yourSecondName) &&
                Objects.equals(yearOfBirth, that.yearOfBirth) &&
                //Objects.equals(noOfCards, that.noOfCards) &&
                Objects.equals(consent, that.consent);
               // && Objects.equals(clinics, that.clinics);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clin, cell, sex, yourFirstName, yourSecondName, yearOfBirth, noOfCards, consent, clinics);
    }


    @Override
    public String toString() {
        return "PatientInfo{" +
                "clin='" + clin + '\'' +
                ", cell='" + cell + '\'' +
                ", sex='" + sex + '\'' +
                ", yourFirstName='" + yourFirstName + '\'' +
                ", yourSecondName='" + yourSecondName + '\'' +
                ", yearOfBirth='" + yearOfBirth + '\'' +
                ", noOfCards='" + noOfCards + '\'' +
                ", consent='" + consent + '\'' +
                ", clinics=" + clinics +
                '}';
    }

}
