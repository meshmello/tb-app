package com.example.mesha.tb_patient_manangement;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class client_reg_form extends AppCompatActivity {
    ArrayList<String> clinicList;
    ArrayList<String> genderList;
    private Spinner clinics;
    private Spinner gender;
    private EditText name;
    private EditText surname;
    private Button addPatient;
    private Button searchPatient;
    private EditText cellNo;
    private EditText yearOfBirth;
    private DatabaseReference roofRef;
    private DatabaseReference mReff;
    private DatabaseReference contact_num;
    private String clinic;
    private EditText noOfCards;
    private CheckBox cons;
    private String checked;
    private List<PatientInfo> patientList= new ArrayList<>();
    private String cli;
    private String no;
    private String altN;
    private EditText altNo;
    private String gender1;
    private String firstName;
    private String secondName;
    private String yob;
    private String nOcards;
    private String logedUser;
    private TextView loged;
    private boolean check;
    private int result;
    private int card;
    private boolean validNumber=true;
    private boolean validALtNumber=true;
    private int y=0;
    private int x=50;

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(getBaseContext(), Menue.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("PatientEamil", logedUser);
        //intent.putExtra("PATIENTID", patientId);
        intent.putExtra("clinic", clinic);

        startActivity(intent);
        finish();

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_reg_form);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        roofRef = FirebaseDatabase.getInstance().getReference();
        mReff= FirebaseDatabase.getInstance().getReference().child("TB");
        contact_num = FirebaseDatabase.getInstance().getReference().child("Contact List");
        mReff.keepSynced(true);
        loged= (TextView) findViewById(R.id.logedMail);

        Intent intent= getIntent();
        logedUser=intent.getStringExtra("PatientEamil");
        clinic = intent.getStringExtra("clinic");
        String number = intent.getStringExtra("number");
        loged.setText("WELCOME "+ logedUser);
        cons=(CheckBox) findViewById(R.id.checkBox);
        cons.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked==true){
                    checked="TRUE";
                    check=true;
                    //Toast.makeText(getApplicationContext(),"true "+isChecked,Toast.LENGTH_SHORT).show();
                }else
                {
                    checked="False";
                    check=false;
                }

            }
        });
        // mReff= new firebase()
        altNo=(EditText) findViewById(R.id.editText4);
        noOfCards=(EditText)findViewById(R.id.editText3);
        clinics= (Spinner) findViewById(R.id.clinicDropDown);
        gender=(Spinner) findViewById(R.id.genderDropdown);
        name= (EditText) findViewById(R.id.names);
        surname= (EditText) findViewById(R.id.surname);
        cellNo= (EditText) findViewById(R.id.cellNo);
        yearOfBirth = (EditText) findViewById(R.id.yearOfBirth);
        addPatient= (Button) findViewById(R.id.clientAddButton);
        searchPatient= (Button) findViewById(R.id.clientSearchButton);
        genderList= new ArrayList<String>();
        genderList.add(0,"--Please select gender--");
        genderList.add("MALE");
        genderList.add("FEMALE");
        genderList.add("INTERSEX");

        if(number != null){
            cellNo.setText(number);
        }

        clinicList = new ArrayList<String>();
        clinicList.add(clinic);

        ArrayAdapter genderAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, genderList);
        ArrayAdapter clinicAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, clinicList);
        clinics.setAdapter(clinicAdapter);
        gender.setAdapter(genderAdapter);
        addPatient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cli= clinics.getSelectedItem().toString();
                no= cellNo.getText().toString();
                gender1= gender.getSelectedItem().toString();
                firstName= name.getText().toString();
                secondName=surname.getText().toString();
                yob = yearOfBirth.getText().toString();
                nOcards=noOfCards.getText().toString();
                altN= altNo.getText().toString();

                Pattern pattern= Pattern.compile("(0)\\d{9}");
                Matcher matcher= pattern.matcher(no);
                Matcher matcher1= pattern.matcher(altN);
                int year = Calendar.getInstance().get(Calendar.YEAR);
                if(yob!=null && !yob.isEmpty()) {
                    result = Integer.parseInt(yob);
                }
                if(nOcards!=null && !nOcards.isEmpty()) {
                    card = Integer.parseInt(nOcards);
                }





                validateNo(no);
                validateAltNo(altN);

//               Toast.makeText(getApplicationContext(), "Please fill in a valid number", Toast.LENGTH_SHORT).show();
                if(!cons.isChecked())
                    Toast.makeText(getApplicationContext(),"You need to give concern in order to continue", Toast.LENGTH_SHORT).show();
                else if(firstName==null && firstName.isEmpty()|| firstName.length()!=3 )
                    Toast.makeText(getApplicationContext(), "First name should have 3 letters", Toast.LENGTH_SHORT).show();
                else  if(secondName==null && secondName.isEmpty()|| secondName.length() != 3)
                    Toast.makeText(getApplicationContext(), "Surname should have 3 letters", Toast.LENGTH_SHORT).show();
                else  if(result<1920 ||result > year)
                    Toast.makeText(getApplicationContext(), "Enter valid year of birth", Toast.LENGTH_SHORT).show();
                else if(gender1.equals("--Please select gender--"))
                    Toast.makeText(getApplicationContext(), "Please select Gender", Toast.LENGTH_SHORT).show();
                else if(!validNumber)
                    Toast.makeText(getApplicationContext(), "Please fill in a valid cell number", Toast.LENGTH_SHORT).show();
                else if(!validALtNumber)
                    Toast.makeText(getApplicationContext(), "Please fill in a valid alt cell number", Toast.LENGTH_SHORT).show();
                else if(nOcards.isEmpty()  || nOcards==null)
                    Toast.makeText(getApplicationContext(), "Please type a positive number between 0 and 50", Toast.LENGTH_SHORT).show();
                else {
                    final PatientInfo patient = new PatientInfo(checked, cli, no, altN, gender1, firstName.toUpperCase(), secondName.toUpperCase(), yob,nOcards, new Date().getTime());
                    Query searchQuery = mReff.orderByChild("cell").equalTo(patient.cell);
                    patientList.clear();
                    searchQuery.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            if(dataSnapshot.exists()){
                                for(DataSnapshot data : dataSnapshot.getChildren()) {

                                    PatientInfo patient = data.getValue(PatientInfo.class);
                                    patientList.add(patient);
                                    Log.d("TB", "Comparing" + patient);
                                }

                            }

                            addPatient(patient);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
                patientList.clear();
            }
        });

        searchPatient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), SearchActivity.class);
                intent.putExtra("clinic", clinic);
                startActivity(intent);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                finish();
            }
        });

    }




    private void addPatient(PatientInfo patient){
        boolean patientExist=false;
        final Toast m_currentToast=Toast.makeText(getBaseContext(),"Patient already exist", Toast.LENGTH_SHORT);

        for(PatientInfo mpatientList : patientList){
            if(patient.equals(mpatientList)){
                patientExist=true;
                break;

            }
            // break;

        }

        if(patientExist){
            int diff = (int) (new Date().getTime() - (long) patient.timestamp);
            int days = (diff / (1000 * 60 * 60 * 24));

            if(days < 30*8) {
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("TB Index Patient");
                alert.setMessage("Patient already exists");
                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        overridePendingTransition(0, 0);
                        startActivity(getIntent());
                        overridePendingTransition(0, 0);
                    }
                });
                alert.create().show();

                patientExist = false;
            }
            else{
                String patientId = mReff.push().getKey();
                mReff.child(patientId).setValue(patient);

                add_to_contact_list(no);

                Intent intent = new Intent(getBaseContext(), contact_register.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("PatientEamil", logedUser);
                intent.putExtra("PATIENTID", patientId);
                intent.putExtra("clinic", clinic);

                startActivity(intent);
                finish();
            }
        }
        else{

            String patientId = mReff.push().getKey();
            mReff.child(patientId).setValue(patient);

            add_to_contact_list(no);

            Intent intent = new Intent(getBaseContext(), contact_register.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("PatientEamil", logedUser);
            intent.putExtra("PATIENTID", patientId);
            intent.putExtra("clinic", clinic);

            startActivity(intent);
            finish();

        }
        m_currentToast.cancel();


    }
    private void validateNo(String number){

        Pattern pattern= Pattern.compile("(0)\\d{9}");
        Matcher matcher= pattern.matcher(number);

        if (number != null && !number.isEmpty()) {  // Or use your favorite library.
            // The user has entered a value: check that it is valid.

            if (!matcher.matches()) {
                //PopMyError();
                // Toast.makeText(getApplicationContext(), "Please fill in a valid number", Toast.LENGTH_SHORT).show();
                validNumber=false;

            }
            else{
                validNumber=true;
            }
        }

    }
    private void validateAltNo(String number){

        Pattern pattern= Pattern.compile("(0)\\d{9}");
        Matcher matcher= pattern.matcher(number);

        if (number != null && !number.isEmpty()) {  // Or use your favorite library.
            // The user has entered a value: check that it is valid.

            if (!matcher.matches()) {
                //PopMyError();
                // Toast.makeText(getApplicationContext(), "Please fill in a valid number", Toast.LENGTH_SHORT).show();
                validALtNumber=false;

            }
            else{
                validALtNumber=true;
            }
        }

    }

    private void add_to_contact_list(final String phoneNumber){
        roofRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String phoneNumber = no;

                int check = 0;

                if(dataSnapshot.child("Contact List").exists()){
                    for (DataSnapshot data : dataSnapshot.child("Contact List").getChildren()) {
                        if(data.getKey().equals(phoneNumber)) {
                            check++;
                            break;
                        }
                    }

                    if(check != 0) {
                        DataSnapshot con = dataSnapshot.child("Contact List").child(phoneNumber);
                        int count = Integer.parseInt(con.child("count").getValue()+"") + 1;

                        contact_num = FirebaseDatabase.getInstance().getReference().child("Contact List");
                        contact_num.child(phoneNumber).child("count").setValue(count);
                        contact_num.child(phoneNumber).child("modified_timestamp").setValue(new Date().getTime());
                        contact_num.child(phoneNumber).child("closed_loop").setValue(true);
                        contact_num.child(phoneNumber).child("status").setValue("Closed at "+cli+" by user, "+logedUser);

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
