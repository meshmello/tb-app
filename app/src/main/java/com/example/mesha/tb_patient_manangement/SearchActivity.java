package com.example.mesha.tb_patient_manangement;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.JsonReader;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {
    private LinearLayout parentLayout;

    private EditText searchClient;
    private Button search;
    private Button addPatient;
    private RecyclerView resultList;

    private DatabaseReference mReff;
    private DatabaseReference clientReference;

    private FirebaseRecyclerAdapter firebaseRecyclerAdapter;
    private String  logedUser;
    private String clinic;

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
        Intent intent = new Intent(getBaseContext(), Menue.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("PatientEamil", logedUser);
        //intent.putExtra("PATIENTID", patientId);
        intent.putExtra("clinic", clinic);

        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        search=(Button)findViewById(R.id.search);
        addPatient=(Button)findViewById(R.id.addPatient);

        searchClient = (EditText)findViewById(R.id.cellNo);

        resultList = (RecyclerView)findViewById(R.id.result_list);
        resultList.setLayoutManager(new LinearLayoutManager(this));
        resultList.addItemDecoration(new DividerItemDecoration(getBaseContext(), DividerItemDecoration.VERTICAL));
        resultList.addItemDecoration(new SpacingItem(getBaseContext(), 16));

        parentLayout = (LinearLayout)findViewById(R.id.searchLayout);
        mReff= FirebaseDatabase.getInstance().getReference().child("TB");

        Intent intent= getIntent();

        clinic = intent.getStringExtra("clinic");
        logedUser=intent.getStringExtra("PatientEamil");

        search.setOnClickListener(new OnClickListener(){

            @Override
            public void onClick(View view) {
                String cell = searchClient.getText().toString();
                if(!cell.equals(""))
                    firebaseClientSearch(cell);
                else
                    Toast.makeText(getApplicationContext(), "Fill in a cell number", Toast.LENGTH_SHORT).show();
            }
        });

        addPatient.setOnClickListener(new OnClickListener(){

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), client_reg_form.class);
                intent.putExtra("clinic", clinic);
                startActivity(intent);
                finish();
            }
        });


    }

    protected void firebaseClientSearch(final String number) {

        Query searchFire = mReff.orderByChild("cell").startAt(number).endAt(number+"\uf8ff");

        FirebaseRecyclerOptions<PatientInfo> options = new FirebaseRecyclerOptions.Builder<PatientInfo>().setQuery(searchFire, PatientInfo.class).build();

        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<PatientInfo, ClientViewHolder>(options)
        {

            @NonNull
            @Override
            public ClientViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

                View view = LayoutInflater.from(parentLayout.getContext()).inflate(R.layout.activity_search_client, parentLayout, false);

                return  new ClientViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull ClientViewHolder holder, final int position, final PatientInfo model) {


                holder.setDetails(model.getCell(), model.getSex(), model.getYourFirstName(), model.getYourSecondName(), model.getYearOfBirth());

                holder.cView.setOnClickListener(new OnClickListener(){

                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getBaseContext(), EditActivity.class);
                        intent.putExtra("client_number", model.getCell());
                        intent.putExtra("client_name", model.getYourFirstName());
                        intent.putExtra("client_surname", model.getYourSecondName());
                        intent.putExtra("client_year", model.getYearOfBirth());
                        intent.putExtra("client_sex", model.getSex());
                        intent.putExtra("client_clinic", model.getClin());

                        intent.putExtra("clinic", clinic);
                        startActivity(intent);
                    }
                });
            }

        };

        resultList.setAdapter(firebaseRecyclerAdapter);

        firebaseRecyclerAdapter.startListening();

        searchFire.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                    System.out.println("clients ex");
                else{
                    Intent intent = new Intent(getBaseContext(), client_reg_form.class);
                    intent.putExtra("number", number);
                    intent.putExtra("clinic", clinic);
                    startActivity(intent);
                    finish();

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    //view holder class
    public class ClientViewHolder extends RecyclerView.ViewHolder{

        View cView;

        public ClientViewHolder(@NonNull View itemView) {
            super(itemView);

            cView = itemView;
        }

        public void setDetails(String cell, String gender, String name, String surname, String dob){
            TextView cell_num = (TextView)cView.findViewById(R.id.textCell);
            TextView gender_ = (TextView)cView.findViewById(R.id.textGender);
            TextView name_ = (TextView)cView.findViewById(R.id.textName);
            TextView surname_ = (TextView)cView.findViewById(R.id.textSurname);
            TextView dob_ = (TextView)cView.findViewById(R.id.textDate);

            cell_num.setText(cell);
            gender_.setText(gender);
            name_.setText(name);
            surname_.setText(surname);
            dob_.setText(dob);
        }
    }

    public class SpacingItem extends RecyclerView.ItemDecoration{
        private int spacing;

        public SpacingItem(Context context, int padding){
            DisplayMetrics metrics = getApplicationContext().getResources().getDisplayMetrics();
            spacing = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, padding, metrics);
        }

        @Override
        public final void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state){
            int position = parent.getChildAdapterPosition(view);
            if(position != state.getItemCount() - 1){
                outRect.bottom = spacing;
            }
        }
    }
}
