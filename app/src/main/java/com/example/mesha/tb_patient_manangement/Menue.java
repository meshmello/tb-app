package com.example.mesha.tb_patient_manangement;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

public class Menue extends AppCompatActivity {
private Button regMenue;
private Button editMenue;
private Button logout;
private TextView texUser;
private TextView texClin;
private String logedUser;
private String clinic;
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
        overridePendingTransition(0, 0);
        startActivity(getIntent());
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menue);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        regMenue= (Button) findViewById(R.id.regBTN);
        editMenue= (Button) findViewById(R.id.editBTN);
        logout= (Button) findViewById(R.id.logoutBTN);
        texUser=(TextView) findViewById(R.id.textViewUser);
        texClin=(TextView) findViewById(R.id.textViewClinicSelect);
        Intent intent= getIntent();
        logedUser=intent.getStringExtra("PatientEamil");
        clinic = intent.getStringExtra("clinic");

        texUser.setText("WELCOME "+ logedUser);
        texClin.setText("You have selected "+ clinic+ " Clinic");
        regMenue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getBaseContext(), client_reg_form.class);
                intent.putExtra("clinic", clinic);
                intent.putExtra("PatientEamil", logedUser);
                startActivity(intent);
                finish();

            }
        });
        editMenue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), SearchActivity.class);
                intent.putExtra("clinic", clinic);
                intent.putExtra("PatientEamil", logedUser);
                startActivity(intent);
                finish();

            }
        });
    logout.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(getBaseContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }
    });

    }
}
