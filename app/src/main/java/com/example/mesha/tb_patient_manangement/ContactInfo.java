package com.example.mesha.tb_patient_manangement;

public class ContactInfo {

    public long first_timestamp;
    public long modified_timestamp;
    public int count;
    public String status;
    public boolean smsed;
    public boolean closed_loop;


    public ContactInfo(){

    }

    public long getFirst_timestamp() {
        return first_timestamp;
    }

    public void setFirst_timestamp(long first_timestamp) {
        this.first_timestamp = first_timestamp;
    }

    public long getModified_timestamp() {
        return modified_timestamp;
    }

    public void setModified_timestamp(long modified_timestamp) {
        this.modified_timestamp = first_timestamp;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean getSmsed() {
        return smsed;
    }

    public void setSmsed(boolean smsed) {
        this.smsed = smsed;
    }

    public boolean getClosed() {
        return closed_loop;
    }

    public void setClosed(boolean closed_loop) {
        this.closed_loop = closed_loop;
    }

    public ContactInfo(long first_timestamp, long modified_timestamp, int count, String status, boolean smsed, boolean closed_loop) {
        this.first_timestamp = first_timestamp;
        this.modified_timestamp = modified_timestamp;
        this.count = count;
        this.status=status;
        this.smsed = smsed;
        this.closed_loop = closed_loop;
    }

}
