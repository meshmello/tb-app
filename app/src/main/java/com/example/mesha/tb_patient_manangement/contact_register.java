package com.example.mesha.tb_patient_manangement;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Selection;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class contact_register extends AppCompatActivity {
    private Button buttonView;
    private Button buttonContinue;
    private EditText edittTxt;

    private LinearLayout parentLayout;
    private int hint=0;
    private Button save;
    private DatabaseReference mReff;
    private DatabaseReference contact_num;
    private DatabaseReference roofRef;
    private  HashMap<String, String> name = new HashMap<String, String>();
    private  HashMap<String, ContactInfo> sms = new HashMap<String, ContactInfo>();
    ArrayList<String> numbers;
    private ArrayList<EditText> editextList= new ArrayList<EditText>();
    private String key;
    private String clinic;
    private String value;
    private String logedUser;
    private String phoneNumber;
    private boolean validNumber=true;
    private List<PatientInfo> patient= new ArrayList<PatientInfo>();
    private FirebaseRecyclerAdapter firebaseRecyclerAdapter;
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(getBaseContext(), Menue.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //intent.putExtra("PatientEamil", logedUser);
        //intent.putExtra("PATIENTID", patientId);
        intent.putExtra("clinic", clinic);
        intent.putExtra("PatientEamil",logedUser);

        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_register);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        buttonView=(Button)findViewById(R.id.buttonView);
        buttonContinue=(Button)findViewById(R.id.buttonContinue);

        parentLayout = (LinearLayout)findViewById(R.id.parentLayout);
        roofRef = FirebaseDatabase.getInstance().getReference();
        mReff = FirebaseDatabase.getInstance().getReference().child("TB");
        roofRef.keepSynced(true);
        mReff.keepSynced(true);

        Intent intent= getIntent();
        key = intent.getStringExtra("PATIENTID");
        clinic = intent.getStringExtra("clinic");
        logedUser=intent.getStringExtra("PatientEamil");


        //Firebase usersRef = ref.child("tbpatientmanagement");
        buttonView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if(validate_add_contact()){
                    createEditTextView();
                }
            }
        });


        buttonContinue.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if(validate_continue()){
                    List nameList = new ArrayList<String>(Arrays.<String>asList(String.valueOf(numbers)));

                    for (int i = 1; i <= editextList.size(); i++) {
                        //edittTxt.getId();

                        phoneNumber = editextList.get(i - 1).getText().toString();

                        //Log.d("idValue", edittTxt.getId()+"");
                        //nameList.add()andrid
                        if (name.containsValue(phoneNumber)) {
                            //Toast.makeText(getApplicationContext(), phoneNumber+" Already exists", Toast.LENGTH_SHORT).show();
                        } else {
                            name.put(("Contact" + i), phoneNumber);
                            // Toast.makeText(getApplicationContext(), phoneNumber + " added", Toast.LENGTH_SHORT).show();
                        }
                        String k="Contact" + i;
                        if(phoneNumber.isEmpty() || phoneNumber == null || phoneNumber.length() < 10){
                            name.remove(k);
                        }

                        if(phoneNumber.length() == 10) {
                            long timestamp = new Date().getTime();
                            ContactInfo potential = new ContactInfo(timestamp, timestamp, 1, "Not smsed", false, false);
                            sms.put(phoneNumber, potential);
                        }
                    }

                    contact_num = FirebaseDatabase.getInstance().getReference().child("Contact List");

                    roofRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for(Map.Entry<String, ContactInfo> entry: sms.entrySet()) {
                                String phoneNumbers = entry.getKey();

                                int check_in_contact_list = 0;
                                int check_in_patient_list = 0;

                                if(dataSnapshot.child("Contact List").exists()){

                                    for (DataSnapshot data : dataSnapshot.child("TB").getChildren()) {
                                        if(data.child("cell").getKey().equals(phoneNumbers)) {
                                            check_in_patient_list++;
                                            break;
                                        }
                                    }

                                    if(check_in_patient_list == 0) {
                                        for (DataSnapshot data : dataSnapshot.child("Contact List").getChildren()) {
                                            if (data.getKey().equals(phoneNumbers)) {
                                                check_in_contact_list++;
                                                break;
                                            }
                                        }
                                        if (check_in_contact_list == 0) {
                                            contact_num.child(phoneNumbers).setValue(entry.getValue());
                                        } else {
                                            DataSnapshot con = dataSnapshot.child("Contact List").child(phoneNumbers);
                                            int diff = (int) (new Date().getTime() - (long) con.child("first_timestamp").getValue());
                                            int days = (diff / (1000 * 60 * 60 * 24));
                                            int count = Integer.parseInt(con.child("count").getValue() + "") + 1;

                                            contact_num.child(phoneNumbers).child("count").setValue(count);

                                            if (days > 30 && (boolean) con.child("smsed").getValue() && !(boolean) con.child("closed_loop").getValue()) {
                                                contact_num.child(phoneNumbers).child("first_timestamp").setValue(new Date().getTime());
                                                contact_num.child(phoneNumbers).child("smsed").setValue(false);
                                            }

                                        }
                                    }
                                }
                                else
                                    contact_num.child(phoneNumbers).setValue(entry.getValue());

                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });


                    if (phoneNumber != null && !phoneNumber.isEmpty()) {
                        validateNo(phoneNumber);
                        if (!validNumber) {
                            Toast.makeText(getApplicationContext(), "Please fill in a valid number", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Phone added successfully", Toast.LENGTH_SHORT).show();
                            DatabaseReference patientRef = FirebaseDatabase.getInstance().getReference().child("TB").child(key);
                            patientRef.child("Contact Numbers").setValue(name);
                            Intent intent = new Intent(getBaseContext(), Menue.class);
                            intent.putExtra("PatientEamil", logedUser);
                            intent.putExtra("clinic", clinic);
                            startActivity(intent);
                            finish();

                        }
                    }
                    else{

                        Toast.makeText(getApplicationContext(), "Phone added successfully", Toast.LENGTH_SHORT).show();
                        DatabaseReference patientRef = FirebaseDatabase.getInstance().getReference().child("TB").child(key);
                        patientRef.child("Contact Numbers").setValue(name);
                        Intent intent = new Intent(getBaseContext(), Menue.class);
                        intent.putExtra("PatientEamil", logedUser);
                        intent.putExtra("clinic", clinic);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        });


    }
    protected void createEditTextView() {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams (
                //RelativeLayout.
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
        );

        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        params.setMargins(0,10,0,10);
        edittTxt = new EditText(this);
        final int maxLength = 10;
        hint++;
        edittTxt.setHint("Contact"+hint);
        edittTxt.setLayoutParams(params);
        // edtTxt.setBackgroundColor(Color.WHITE);
        edittTxt.setInputType(InputType.TYPE_CLASS_PHONE);
        edittTxt.setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
        edittTxt.setId(hint);
        Selection.setSelection((Editable) edittTxt.getText(),edittTxt.getSelectionStart());
        edittTxt.requestFocus();
        editextList.add(edittTxt);
        final InputFilter[] fArray = new InputFilter[1];
        fArray[0] = new InputFilter.LengthFilter(maxLength);
        edittTxt.setFilters(fArray);
        numbers= new ArrayList<String>();


        parentLayout.addView(edittTxt);
        mReff.child("TB").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                for (DataSnapshot child:children) {
                    PatientInfo patientInfo = child.getValue(PatientInfo.class);
                    patient.add(patientInfo);

                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String prevChildKey) {
                PatientInfo changedPost = dataSnapshot.getValue(PatientInfo.class);
                System.out.println("The updated post title is: " + changedPost.cell);



            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {}

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String prevChildKey) {
                //key= dataSnapshot.getKey().toString();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }
    private void validateNo(String number){

        Pattern pattern= Pattern.compile("(0)\\d{9}");
        Matcher matcher= pattern.matcher(number);

        if (number != null && !number.isEmpty()) {  // Or use your favorite library.
            // The user has entered a value: check that it is valid.

            if (!matcher.matches()) {
                //PopMyError();
                // Toast.makeText(getApplicationContext(), "Please fill in a valid number", Toast.LENGTH_SHORT).show();
                validNumber=false;

            }
            else{
                validNumber=true;
            }
        }

    }

    private boolean validate_add_contact(){
        int counter = editextList.size();
        int present = 0;

        if (counter == 0){
            return true;
        }
        else if(counter<10) {
            String number = edittTxt.getText().toString();
            for (int i = 0; i < editextList.size(); i++) {
                if(editextList.get(i).getText().toString().equals(number) && i != editextList.size()-1){
                    present++;
                    break;
                }
            }

            if(present == 0) {
                if (number.length() == 10) {
                    Pattern pattern= Pattern.compile("(0)\\d{9}");
                    Matcher matcher= pattern.matcher(number);

                    if (!matcher.matches()) {
                        Toast.makeText(getApplicationContext(), "Please enter a 10 digit cell number", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                    else{
                        return true;
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Please enter a 10 digit cell number", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
            else {
                present = 0;
                Toast.makeText(getApplicationContext(), "Number has already been entered above", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        else {
            Toast.makeText(getApplicationContext(), "Maximum Numbers reached", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private boolean validate_continue(){
        int counter = editextList.size();
        int present = 0;

        if (counter == 0){
            return true;
        }
        else if(counter<10) {
            String number = edittTxt.getText().toString();
            for (int i = 0; i < editextList.size(); i++) {
                if(editextList.get(i).getText().toString().equals(number) && i != editextList.size()-1){
                    present++;
                    break;
                }
            }

            if(present == 0) {
                if (number.length() == 10) {
                    Pattern pattern= Pattern.compile("(0)\\d{9}");
                    Matcher matcher= pattern.matcher(number);

                    if (!matcher.matches()) {
                        Toast.makeText(getApplicationContext(), "Please enter a 10 digit cell number", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                    else{
                        return true;
                    }

                }
                else if (number.length() == 0)
                    return true;
                else {
                    Toast.makeText(getApplicationContext(), "Please enter a 10 digit cell number", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
            else {
                present = 0;
                Toast.makeText(getApplicationContext(), "Number has already been entered above", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        else {
            Toast.makeText(getApplicationContext(), "Maximum Numbers reached", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

}
