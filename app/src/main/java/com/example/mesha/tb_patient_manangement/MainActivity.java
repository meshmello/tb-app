package com.example.mesha.tb_patient_manangement;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button login;
    private Button register;
    private DatabaseReference mReff;
    private EditText email;
    private EditText password;
    private FirebaseAuth mAuth;
    private String logedUser;
    private FirebaseAuth.AuthStateListener mAuthListner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mReff= FirebaseDatabase.getInstance().getReference();
        login= (Button) findViewById(R.id.loginButton);
        register = (Button) findViewById(R.id.registerButton);
        register.setOnClickListener(this);
        email= (EditText) findViewById(R.id .emailField);
        password= (EditText) findViewById(R.id.passwordField);
        mAuth=FirebaseAuth.getInstance();
        mAuthListner = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if(firebaseAuth.getCurrentUser()!=null){
                    //intent
                    logedUser=firebaseAuth.getCurrentUser().getEmail().toString();

                }

            }
        };


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch(v.getId()) {

                    case R.id.loginButton:
                        userLogin();
                        break;
                }
            }
        });
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch(v.getId()){
                    case R.id.registerButton :
                        Intent intent = new Intent(MainActivity.this, Registration.class);
                        startActivity(intent);
                        finish();
                        break;

                }

            }
        });
    }

    private void userLogin(){

        final String loginEmail= email.getText().toString().trim();
        String loginPassword= password.getText().toString().trim();
        if(loginEmail.isEmpty()){
            email.setError("Email is required");
            email.requestFocus();
            return;
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(loginEmail).matches()){
            email.setError("Please enter a valid email address");
            email.requestFocus();
            return;
        }
        if(loginPassword.isEmpty()){
            password.setError("Password is required");
            password.requestFocus();
            return;
        }
        if(password.length()<6){
            password.setError("password minimum length should be 6");
            password.requestFocus();
            return;
        }
        mAuth.signInWithEmailAndPassword(loginEmail,loginPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Intent intent = new Intent(MainActivity.this, ClinicSelection.class);
                    logedUser=loginEmail.toString();
                    intent.addFlags(Intent .FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("PatientEamil", logedUser);
                    startActivity(intent);
                    finish();
                }else{

                    Toast.makeText(getApplicationContext(),task.getException().getMessage(),Toast.LENGTH_SHORT).show();

                }

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListner);
    }

    @Override
    public void onClick(View v) {

    }
}
