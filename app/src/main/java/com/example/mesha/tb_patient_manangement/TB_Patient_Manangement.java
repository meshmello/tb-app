package com.example.mesha.tb_patient_manangement;

import android.app.Application;

import com.google.firebase.database.FirebaseDatabase;

public class TB_Patient_Manangement extends Application {
    @Override
    public void onCreate(){
        super.onCreate();

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}
