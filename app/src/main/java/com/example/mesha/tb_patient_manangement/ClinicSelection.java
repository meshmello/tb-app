package com.example.mesha.tb_patient_manangement;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ClinicSelection extends AppCompatActivity {
    private CustomSpinner clinics;
    private Spinner province;
    private TextView loged;
    private DatabaseReference provinceReference;
    private DatabaseReference clinicReference;

    private String clinicSelected;
    private String logedUser;

    private int selected = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic_selection);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        provinceReference = FirebaseDatabase.getInstance().getReference().child("Clinics");
        Intent intent= getIntent();
        logedUser=intent.getStringExtra("PatientEamil");

        province = (Spinner) findViewById(R.id.provincesList);
        loged= (TextView) findViewById(R.id.userLog);
        loged.setText("WELCOME " + logedUser);

        province.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String provinceSelected = parent.getItemAtPosition(position).toString();
                clinicReference = FirebaseDatabase.getInstance().getReference().child("Clinics");
                selected = 0;
                populateClinics(getBaseContext(), provinceSelected);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        clinics = findViewById(R.id.clinicList);

        clinics.setOnItemSelectedEvenIfUnchangedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                clinicSelected = parent.getItemAtPosition(position).toString();

                selected++;

                if(selected > 1 && !clinicSelected.equals("---Please select Clinic---")) {
                    //Intent intent = new Intent(getBaseContext(), client_reg_form.class);
                    Intent intent = new Intent(getBaseContext(), Menue.class);
                    intent.putExtra("clinic", clinicSelected);
                    intent.putExtra("PatientEamil", logedUser);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Spinner for province
        populateProvinces(this.getBaseContext());

        //Spinner for clinics
    }

    public void populateProvinces(final Context text){
        final ArrayList<String> provinceList =  new ArrayList<String>();
        provinceList.add(0, "-----Please select Province-----");

        provinceReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                fetchProvinces(dataSnapshot, provinceList);

                ArrayAdapter<String> provinceAdapter = new ArrayAdapter<String>(text, R.layout.spinner_item, provinceList);
                provinceAdapter.setDropDownViewResource(R.layout.spinner_drop_item);
                province.setAdapter(provinceAdapter);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                //fetchDta(dataSnapshot,clinicLists);
                fetchProvinces(dataSnapshot, provinceList);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void populateClinics(final Context text, final String provinceSelected){
        final ArrayList<String> clinicLists =  new ArrayList<String>();
        clinicLists.add(0, "---Please select Clinic---");

        clinicReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                fetchClinics(dataSnapshot, provinceSelected, clinicLists);

                ArrayAdapter<String> clinicAdapter = new ArrayAdapter<String>(text, R.layout.spinner_item, clinicLists);
                clinicAdapter.setDropDownViewResource(R.layout.spinner_drop_item);
                clinics.setAdapter(clinicAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void fetchProvinces(DataSnapshot snapshot,ArrayList<String> provinceList){

        provinceList.add(snapshot.getKey());
    }

    private void fetchClinics(DataSnapshot snapshot, String provinceSelected, ArrayList<String> clinicLists){
        for(DataSnapshot clinics: snapshot.child(provinceSelected).getChildren()){

            clinicLists.add(clinics.getValue().toString());
        }
    }



}