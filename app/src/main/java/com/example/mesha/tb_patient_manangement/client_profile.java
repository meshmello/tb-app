package com.example.mesha.tb_patient_manangement;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.util.Log;


import java.util.ArrayList;

public class client_profile extends AppCompatActivity {
    private Button registerNew;
    private Button editPatient;
    final String TAG = "myApp";
    String clinic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_profile);

        Intent intent= getIntent();

        clinic = intent.getStringExtra("clinic");
    }

        public void addClient(View view){
            Intent intent1 = new Intent(this, client_reg_form.class);
            Log.v(TAG, "intent initialised");
            startActivity(intent1);
    }
}
